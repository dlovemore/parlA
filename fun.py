import operator
from functools import reduce,partial
inst=isinstance

class Ops:
    def __neg__(S): return S._opr('neg',S)
    def _opR(S,op,rhs): return S._opr(op,S         ,S._lf(rhs))
    def _Lop(S,op,lhs): return S._opr(op,S._lf(lhs),S         )
    def      __lt__ (S,rhs): return S._opR('lt' ,rhs)
    def      __gt__ (S,rhs): return S._opR('gt' ,rhs)
    def      __le__ (S,rhs): return S._opR('le' ,rhs)
    def      __ge__ (S,rhs): return S._opR('ge' ,rhs)
    def      __or__ (S,rhs): return S._opR('or' ,rhs)
    def      __and__(S,rhs): return S._opR('and',rhs)
    def      __add__(S,rhs): return S._opR('add',rhs)
    def     __radd__(S,lhs): return S._Lop('add',lhs)
    def      __sub__(S,rhs): return S._opR('sub',rhs)
    def     __rsub__(S,lhs): return S._Lop('sub',lhs)
    def      __mod__(S,rhs): return S._opR('mod',rhs)
    def     __rmod__(S,lhs): return S._Lop('mod',lhs)
    def      __mul__(S,rhs): return S._opR('mul',rhs)
    def     __rmul__(S,lhs): return S._Lop('mul',lhs)
    def   __matmul__(S,rhs): return S._opR('matmul',rhs)
    def  __rmatmul__(S,lhs): return S._Lop('matmul',lhs)
    def  __truediv__(S,rhs): return S._opR('div',rhs)
    def __rtruediv__(S,lhs): return S._Lop('div',lhs)
    def  __getitem__(S,rhs): return S._opR('idx',rhs)
    def __call__(S,*args): return S._opr('call',S,*args)
    def  __pow__(S,*args): return S._opr('pow',S,*args)

oppre={'neg'}
opprec={'call':2,'idx':2,'neg':2,
    'add':22,'sub':22,'mod':14,'mul':14,'matmul':14,'div':14,
    'pow':10,
    'lt':70,'gt':70,'le':70,'ge':70,
    'and':50,'or':60,')':999}
opsym={'call':'(','idx':'[','neg':'-',
    'add':'+','sub':'-','mod':'%','mul':'*','matmul':'@','div':'/',
    'pow':'**',
    'lt':'<','gt':'>','le':'<=','ge':'>=',
    'and':'&','or':'|'}
opsuf={'call':')','idx':']'}

def repx(x): return hasattr(x, '_repx') and x._repx() or (repr(x),0)

def lift(val): return L(*val) if inst(val,set) else val

class Expr(Ops):
    def __init__(S,op,*args):
        S._op=op
        S._args=args
    def _opr(S,*opnargs): return type(S)(*opnargs)
    def _lf(S,val): return lift(val)
    def _repx(S):
        if S._op in opprec:
            prec=opprec[S._op]
            if len(S._args)==1:
                r,prec0=repx(S._args[0])
                if prec0 > prec: r='('+r+')'
                r=opsym[S._op]+r
                return r, prec
            if len(S._args)==2:
                r0,prec0 = repx(S._args[0])
                if prec0 > prec: r0='('+r0+')'
                r1,prec1 = repx(S._args[1])
                if prec1 >= prec: r1='('+r1+')'
                return r0+opsym[S._op]+r1+opsuf.get(S._op,''), prec
        args=','.join(map(repr,[S._op]+list(S._args)))
        return f'{type(S).__name__}({args})', 0
    def __repr__(S):
        r,prec=S._repx()
        return r

opbin={'+':'add','-':'sub','%':'mod','*':'mul','@':'matmul','/':'div',
    '(':'par',')':')',
    '<':'lt','>':'gt','<=':'le','>=':'ge',
    '|':'or','&':'and','>':'gt'}
opuna={'-':'neg'}
def parse(toks,i=0,prec=999):
    lhs,i = toks[i],i+1
    if lhs in opuna:
        op=opuna[lhs]
        lhs,i = parse(toks,i,0)
        lhs=Expr(op,lhs)
        return lhs,i
    if lhs=='(':
        lhs,i = parse(toks,i,99)
        if toks[i]!=')':return 'missing )', i
        i+=1
    while i<len(toks):
        op=opbin[toks[i]]
        rprec=opprec[op]
        if rprec >= prec: return lhs, i
        rhs,i = parse(toks,i+1,rprec)
        lhs=Expr(op,lhs,rhs)
    return lhs,i

def token(s): return s.strip().split()

class Var(Expr):
    def _opr(S,*opnargs): return Expr(*opnargs)
    @property
    def _name(S): return S._op
    def _repx(S): return S._name, 0

class VClass:
    def __getattr__(S,name):
        return Var('name')
V=VClass()


class Fail(Ops):
    def __init__(S,val):
        S.val=val
    def _opr(S,op,*args): return Fail(Expr(op,S,*args))
    def _lf(S,val): return lift(val)
    def __or__(S,rhs): return S.val*rhs
    def __and__(S,rhs): return S
    def __mul__(S,rhs): return inst(rhs,Fun) and rhs(S) or S
    def __bool__(S): return False
    def __repr__(S):
        return f'⊥({S.val})'

def unwrap(args): return len(args)==1 and args[0] or args
def wrap(args): return args if inst(args, tuple) else (args,)

class Fun:
    def call(S,*args): return Expr("call",S,*args)
    def _lf(S,val): return lift(val)
    def __mul__(S,rhs):
        return inst(rhs,Fun) and Compose('mul',S,rhs) or NotImplemented
    def __ror__(S,lhs): return lhs
    def __rand__(S,lhs): return lhs*S
    def __or__(S,rhs):
        return inst(rhs,Fun) and Compose('or',S,rhs) or NotImplemented
    def __and__(S,rhs):
        return inst(rhs,Fun) and Compose('and',S,rhs) or NotImplemented

composeops={'mul':operator.mul,'or':operator.or_}
def compose(opname,funs):
    op=composeops[opname]
    return partial(reduce, op, funs)

class Compose(Fun):
    def __init__(S,opname,*funs):
        S.op=opname
        S.funs=funs
    def _opr(S,op,rhs):
        return S.op==op and (
                inst(rhs,Compose) and Compose(op,*(S.funs+rhs.funs)) or
                inst(rhs,Fun)     and Compose(op,*(S.funs+(rhs,))) or
                NotImplemented) or (
                    inst(rhs,Fun) and Compose(op,S,rhs))
    def __rmul__(S,lhs): return compose(S.op,S.funs[1:])(lhs*S.funs[0])
    def __mul__(S,rhs): return S._opr('mul',rhs)
    def __or__ (S,rhs): return S._opr('or',lift(rhs))
    def __repr__(S): return opsym[S.op].join(map(repr,S.funs))

class PyFun(Fun):
    def __init__(S,fun,name=None):
        S.fun=fun
        S.name=name or hasattr(fun, '__name__') and fun.__name__
    def __call__(S, *args): return S.fun(*args)
    def __rmul__(S,lhs): return S.fun(*wrap(lhs))
    def __repr__(S): return S.name or repr(S.fun)

class Lambda(Fun):
    def __init__(S,head,body):
        S.head=head
        S.body=body
    @property
    def parm(S): return S.head.parm
    def __call__(S,*args):
        if any(inst(arg,Expr) for arg in args):
            return Expr("call",S,*args)
        return Feval(S, unwrap(args))
    def __rmul__(S,lhs): return Feval(S, lhs)
    def __repr__(S):
        bs=repr(S.body)
        if S.head.parm==A: return f'{{{bs}}}'
        if bs[:1]+bs[-1:]!='()': bs=f'({bs})'
        return f"{S.head}{bs}"

class Prop(Fun):
    def __init__(S,attr):
        S.attr=attr
    def __call__(S,arg):
        return getattr(arg,S.attr) if hasattr(arg,S.attr) else Fail(arg)
    def __rmul__(S,lhs): return S.__call__(lhs)
    def __repr__(S): return f'prop.{S.attr}'

class PropClass:
    def __getattr__(S,attr): return Prop(attr)
    def __repr__(S): return 'prop'

prop=PropClass()

def reprparm(parm):
    r=repr(parm)
    if inst(parm,tuple) and r: r=','.join(map(repr,parm))
    return r

class HeadLamb:
    def __init__(S,parm):
        S.parm=parm
    def __call__(S, *body):
        return Lambda(S,(body[0] if len(body)==1 else body))
    def __repr__(S): return f'λ[{reprparm(S.parm)}]'

class Match(Fun):
    def __init__(S,parm):
        S.parm=parm
    def __rmul__(S,lhs): return Meval(S.parm, lhs)
    def __repr__(S): return f'M[{reprparm(S.parm)}]'

class MClass:
    def __getitem__(S, parm): return Match(parm)
    def __repr__(S): return f'M'
M=MClass()

class LambClass:
    def __getitem__(S, parm): return HeadLamb(parm)
    def __repr__(S): return f'λ'
lamb=λ=LambClass()

A=Var('A')
X,Y,Z,W=A[0],A[1],A[2],A[3]
L=lamb[A]
class AClass(Fun):
    def __init__(S,name):
        S._name=name
        S._son={}
        S._inp=None
        S._out=None
        S._f=L(A)
        S._dad=None
    def __call__(S,body):
        S._f=L(body)
        return S
    def __getattr__(S,name):
        if name in S._son: return S._son[name]
        if name.startswith('_'): raise AttributeError
        r=type(S)(name)
        S._son[name]=r
        r._dad=S
        return r
    def __rmul__(S,lhs):
        S._inp=lhs
        S._out=S._inp*S._f
        return S._out
    def __rfloordiv__(S,lhs):
        if inst(lhs,set) or inst(lhs,Fun):
            S._f=lift(lhs)
            return S._f
        return NotImplemented
    def _repx(S):
        name=S._name
        dad=S._dad
        while dad:
            name=dad._name+'.'+name
            dad=dad._dad
        return f'{name}', 0
    def __str__(S):
        name,prec = S._repx()
        return f'{S._f}//{name}'
    def __repr__(S):
        name,prec = S._repx()
        return name
Tap=AClass('Tap')
F=AClass('F')
K=AClass('K')


class KV(Fun):
    def __init__(S,d=()):
        S.d=dict(d)
    def __call__(S,k):
        return S.d[k] if k in S.d else Fail(k)
    def __rmul__(S,lhs):
        return S(*wrap(lhs))
    @classmethod
    def union(cls,kv0,kv1):
        r=KV(kv0.d)
        r.d.update(kv1.d)
        return r
    def __or__(S,rhs):
        # a|b is similar to b|a in Python 3.9
        if inst(rhs,KV): return KV.union(rhs,S)
        if inst(rhs,dict): return KV.union(KV(rhs),S)
        return super().__or__(rhs)
    def __ror__(S,lhs):
        if inst(lhs,dict): return KV.union(S,KV(lhs))
        return super().__ror__(lhs)
    def __repr__(S): return f'{type(S).__name__}({S.d})'

def operate(opname):
    f=getattr(operator,opname)
    def op(env,*args): return f(*args)
    return op

def retarg(arg):
    def v(env): return arg
    return v

def bilt(env,a,b): return a if a< b else Fail(a)
def bigt(env,a,b): return a if a> b else Fail(a)
def bile(env,a,b): return a if a<=b else Fail(a)
def bige(env,a,b): return a if a>=b else Fail(a)

def bior (env,a,f): return a.val*f if inst(a,Fail) else a
def biand(env,a,f): return a       if inst(a,Fail) else a*f

ops=['add','sub','mod','mul','matmul','pow','neg']
builtins={s:operate(s) for s in ops}
builtins.update({'div':operate('truediv')})
builtins.update({'idx':operate('getitem')})
builtins.update({'lt':bilt,'gt':bigt,'le':bile,'ge':bige})
builtins.update({'or':bior,'and':biand})

def union(sets): return reduce(operator.or_, sets)

def vars(e):
    if inst(e, Var): return {e._name}
    elif inst(e, Expr): return union(map(vars,e._args))
    elif inst(e, tuple):return union(map(vars,e))
    elif inst(e, list): return union(map(vars,e))
    return set()

def Leval(bnd, env, e):
    if inst(e, Var):
        if e._name in bnd: return e
        return env(e._name)(env)
    elif inst(e, Lambda):
        head=e.head
        bnd2=vars(head.parm)
        return head(Leval(bnd|bnd2,env,e.body))
    elif inst(e, Expr):
        return type(e)(e._op,*(Leval(bnd,env,se) for se in e._args))
    elif inst(e, tuple): return tuple(Leval(bnd,env,se) for se in e)
    elif inst(e, list ): return list (Leval(bnd,env,se) for se in e)
    return e


def Meval(parm,arg):
    name2arg=dict()
    def match(e,arg):
        if inst(e, Var):
            if e._name in name2arg:
                return name2arg[e._name]==arg or Fail(arg)
            name2arg[e._name]=arg
            return True
        elif e==arg: return True
        elif inst(e, Expr):
            return (inst(arg,Expr) and match(e._op,arg._op) and
                    all(map(match,e._args,arg._args)))
            return False
        elif inst(e, tuple) or inst(e, list):
            return inst(arg,type(e)) and all(map(match,e,arg))
        elif inst(e,slice):
            return inst(arg, e.stop) and match(e.start,arg)
        return False
    return name2arg if match(parm,arg) else Fail(arg)

def Fmatch(parm,arg):
    name2arg=dict()
    def match(e,arg):
        if inst(e, Var):
            name2arg[e._name]=retarg(arg)
            return True
        elif e==arg: return True
        elif inst(e, Expr):
            return (inst(arg,Expr) and match(e._op,arg._op) and
                    all(map(match,e._args,arg._args)))
            return False
        elif inst(e, tuple) or inst(e, list):
            return inst(arg,type(e)) and all(map(match,e,arg))
        return False
    return name2arg if match(parm,arg) else Fail(arg)

def Feval(fun,arg,env=KV(builtins)):
    name2arg = Fmatch(fun.parm,arg)
    if inst(name2arg, Fail): return name2arg
    env=name2arg|env
    def ev(env,e):
        if inst(e, Lambda): return Leval(set(),env,e)
        elif inst(e, Expr):
            return env(e._op)(env,*(ev(env,se) for se in e._args))
        elif inst(e, tuple): return tuple(ev(env,se) for se in e)
        elif inst(e, list): return list(ev(env,se) for se in e)
        return e
    return ev(env,fun.body)

F.ev
lamb[V.env,X](X*(
    # M[X:Lambda]&L((set(),V.env,X)*F.Leval)|
    M[X:Var   ]&L((V.env)(X))|{0}))

(
M[X:Expr  ]&L((X*prop.op*V.env)*L(A*(X*prop._args@L(V.env,A)@F.ev)))|
M[X:tuple ]&L(X@F.ev)|
M[X:list  ]&L(X@F.ev)|
# L(X))//F.ev)
{0})

F.Leval
lamb[V.bnd,V.env,X](X*(
    M[X:Var]))

# >>> from parlA.fun import *
# >>> Var('X')==Var('X')
# False
# >>> X=Var('X')
# >>> X
# X
# >>> X+1
# X+1
# >>> X-X-1
# X-X-1
# >>> X-(X-1)
# X-(X-1)
# >>> X*X+X+X*X
# X*X+X+X*X
# >>> X*(X+X)*X
# X*(X+X)*X
# >>> 
# >>> X*5
# X*5
# >>> X@2
# X@2
# >>> X%5
# X%5
# >>> 2@X
# 2@X

# >>> 
# >>> 
# >>> parse([1,'+',2,'+',33,'*','(',4,'-',6,')'])
# (1+2+33*(4-6), 11)
# >>> ps=PyFun(token)*PyFun(parse)
# >>> ps("1")
# <19>:1: TypeError: 'Compose' object is not callable
# >>> 
# >>> "a + b * ( c - d ) * ( e )"*ps
# ('a'+'b'*('c'-'d')*'e', 13)
# >>> "2 + 3 * ( 4 - 5 ) * ( 6 ) + 7"*ps
# ('2'+'3'*('4'-'5')*'6'+'7', 15)
# >>> "- ( - - ( ( X + 1 ) ) )"*ps
# (---('X'+'1'), 12)

# >>> KV()
# KV({})
# >>> 
# >>> KV({1:2})
# KV({1: 2})
# >>> _(1)
# 2
# >>> KV({1:2,2:3})|KV({1:-1,3:6})
# KV({1: 2, 3: 6, 2: 3})
# >>> d=_
# >>> d(1)
# 2
# >>> d(3)
# 6
# >>> d(5)
# ⊥(5)
# >>> d(2)
# 3
# >>> d(5)|L(0)
# 0
# >>> 5*(d|L(0))
# 0

# >>> X=Var('X')
# >>> Var('x')
# x
# >>> Expr(2)+3
# Expr(2)+3
# >>> 
# >>> lamb[X]
# λ[X]
# >>> lamb[X](1)
# λ[X](1)
# >>> 2*_
# 1
# >>> lamb[X](X)
# λ[X](X)
# >>> 0*_
# 0
# >>> 
# >>> f=lamb[X](X+1)
# >>> f
# λ[X](X+1)
# >>> 2*f
# 3
# >>> f(4)
# 5

# >>> X=Var('X')
# >>> Y=Var('Y')
# >>> lamb[X,Y]
# λ[X,Y]
# >>> lamb[X,Y](X+Y)
# λ[X,Y](X+Y)
# >>> (2,3)*_
# 5
# >>> a=lamb[X,Y](X+Y,X-Y)
# >>> a
# λ[X,Y](X+Y, X-Y)
# >>> (1,2)*a
# (3, -1)
# >>> b=lamb[X,Y](X*Y)
# >>> (5,3)*a
# (8, 2)
# >>> _*b
# 16
# >>> (5,3)*a*b
# 16
# >>> a(5,3)*b
# 16
# >>> a*b
# λ[X,Y](X+Y, X-Y)*λ[X,Y](X*Y)
# >>> f=a*b
# >>> (5,3)*f.funs[0]
# (8, 2)
# >>> _*f.funs[1]
# 16
# >>> 
# >>> f.funs[0](5,3)
# (8, 2)
# >>> f.funs[1](8,2)
# 16
# >>> (5,3)*f
# 16
# >>> 
# >>> f(5,3)
# <72>:1: TypeError: 'Compose' object is not callable
# >>> 
# >>> lamb[X](X+1)(Y)
# λ[X](X+1)(Y)
# >>> Y*lamb[X](X+1)
# Y*λ[X](X+1)
# >>> 

# >>> g=lamb[X](X+1)*lamb[X](X*2)
# >>> g
# λ[X](X+1)*λ[X](X*2)
# >>> type(g)
# <class 'parlA.fun.Compose'>
# >>> g.funs
# (λ[X](X+1), λ[X](X*2))
# >>> g.funs[0](1)
# 2
# >>> g.funs[1](2)
# 4
# >>> 1*g
# 4
# >>> 
# >>> d=lamb[2](3)|lamb[4](5)
# >>> d
# λ[2](3)|λ[4](5)
# >>> (1,2)*d
# ⊥((1, 2))
# >>> 1*d
# ⊥(1)
# >>> 2*d
# 3
# >>> 3*d
# ⊥(3)
# >>> 4*d
# 5

# >>> f=PyFun(sum)
# >>> f
# sum
# >>> 
# >>> sum((1,2,3))
# 6
# >>> f((1,2,3))
# 6
# >>> add=PyFun(operator.add)
# >>> add
# add
# >>> add(1,2)
# 3
# >>> unwrap((1,2))
# (1, 2)
# >>> (2,3)*add
# 5
# >>> ((2,3),)*f
# 5
# >>> f((2,3))
# 5
# >>> div=PyFun(operator.truediv,'div')
# >>> div
# div
# >>> (113,355)*div
# 0.3183098591549296
# >>> swap=lamb[X,Y](Y,X)
# >>> swap*div
# λ[X,Y](Y, X)*div
# >>> (113,355)*_
# 3.1415929203539825
# >>> (113,355)*swap*div
# 3.1415929203539825
# >>> 
# >>> 

# >>> X<3
# X<3
# >>> f=lamb[X](X<3)
# >>> 2*f,4*f
# (2, ⊥(4))
# >>> X<4|2
# X<6
# >>> (X<4)|2
# (X<4)|2
# >>> 
# >>> X&3
# X&3
# >>> min=lamb[X,Y]((X<Y)|{Y})
# >>> min
# λ[X,Y]((X<Y)|{Y})
# >>> vars(min.head.parm)
# {'X', 'Y'}
# >>> 
# >>> (3,4)*min
# 3
# >>> (4,3)*min
# 3
# >>> max=lamb[X,Y]((X>Y)|{Y})
# >>> (3,4)*max
# 4
# >>> (4,3)*max
# 4
# >>> div=lamb[X,0]('Division by Zero')|lamb[X,Y](X/Y)
# >>> type(div)
# <class 'parlA.fun.Compose'>
# >>> div.op
# 'or'
# >>> (2,3)*div
# 0.6666666666666666
# >>> 
# >>> 
# >>> (2,0)*div
# 'Division by Zero'
# >>> 1*lamb[0](2)
# ⊥(1)
# >>> 

# >>> X/3
# X/3
# >>> 4*lamb[X](X/3)
# 1.3333333333333333
# >>> X<=4
# X<=4
# >>> LX=lamb[X]
# >>> 4*LX(X<=4&{1})
# <142>:1: TypeError: unsupported operand type(s) for &: 'int' and 'set'
# >>> 4*LX((X<=4)&{1})
# 1
# >>> 5*LX((X<=4)&{1})
# ⊥(5)
# >>> 5*LX((X<=4)&L(1))
# ⊥(5)
# >>> 3*LX(X<=4)&LX(1)
# 1
# >>> 
# >>> X[4]
# X[4]
# >>> (1,2,3)*LX(X[1])
# 2
# >>> 2>X
# X<2
# >>> 2<=X
# X>=2
# >>> 1*LX(2<=X)
# ⊥(1)
# >>> 2*LX(2<=X)
# 2
# >>> -X
# -X
# >>> {A*A*A}//F.cube
# {A*A*A}
# >>> 3*F.cube
# 27
# >>> 

# >>> Tap.a
# Tap.a
# >>> 2*Tap.a
# 2
# >>> Tap.a(A-1)
# Tap.a
# >>> 2*Tap.a
# 1
# >>> print(Tap.a)
# {A-1}//Tap.a
# >>> F
# F
# >>> lamb[X](X**.5)//F.sqrt
# λ[X](X**0.5)
# >>> F.sqrt
# F.sqrt
# >>> print(F.sqrt)
# λ[X](X**0.5)//F.sqrt
# >>> 
# >>> 2*F.sqrt
# 1.4142135623730951
# >>> Tap.a
# Tap.a
# >>> A*F.sqrt
# A*F.sqrt
# >>> F.sqrt._inp,F.sqrt._out
# (2, 1.4142135623730951)
# >>> swap//F.swap
# λ[X,Y](Y, X)
# >>> (1,2)*F.swap
# (2, 1)
# >>> 2*F.neg(-A)
# -2
# >>> F.a*F.b//F.c
# F.a*F.b
# >>> 2*F.c
# 2
# >>> F.a(A*A)
# F.a
# >>> F.b(A+A)
# F.b
# >>> 2*F.c
# 8
# >>> 3*F.c
# 18
# >>> F.c(4)
# F.c
# >>> F.b*F.b*F.b//F.b3
# F.b*F.b*F.b
# >>> F.b3._f.funs
# (F.b, F.b, F.b)
# >>> 3*F.b3
# 24
# >>> F.a*F.a*PyFun(print)
# F.a*F.a*print
# >>> 9*_,3*_
# 6561
# 81
# (None, None)
# >>> 
# >>> 9**4
# 6561
# >>> F.pos(A>0)
# F.pos
# >>> print(_)
# {A>0}//F.pos
# >>> 
# >>> 2*F.pos
# 2
# >>> -2*F.pos
# ⊥(-2)
# >>> F.pos|F.a
# F.pos|F.a
# >>> _//F.pp
# F.pos|F.a
# >>> 2*F.pp
# 2
# >>> -2*F.pp
# 4
# >>> print(F.pp)
# F.pos|F.a//F.pp
# >>> print(F.pos)
# {A>0}//F.pos
# >>> 1//F.pos
# <200>:1: TypeError: unsupported operand type(s) for //: 'int' and 'AClass'
# >>> 

# >>> M[1]
# M[1]
# >>> 2*_
# ⊥(2)
# >>> 1*M[1]
# {}
# >>> 1*M[X,X]&L('true')|L('false')
# 'false'
# >>> (2,3)*M[X,X]&L('true')|L('false')
# 'false'
# >>> (3,3)*M[X,X]&L('true')|L('false')
# 'true'
# >>> 
# >>> 0*M[0]&L(3)
# 3
# >>> 0*M[1]|L(2)
# 2
# >>> (2,3)*M[X,X]
# ⊥((2, 3))
# >>> (2,2)*M[X,X]
# {'X': 2}
# >>> 

# >>> bool(Fail(2))
# False
# >>> Fail(2) and 1
# ⊥(2)
# >>> Fail(2) or 1
# 1
# >>> X*prop.name
# X*prop.name
# >>> ''*prop.split
# <built-in method split of str object at 0xb676c960>
# >>> 
# >>> 
